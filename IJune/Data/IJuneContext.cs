using IJune.Models;
using Microsoft.EntityFrameworkCore;

namespace IJune.Data
{
    public class IJuneContext : DbContext
    {
        public DbSet<Album> Albums {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Album.db");
        }
    }
}