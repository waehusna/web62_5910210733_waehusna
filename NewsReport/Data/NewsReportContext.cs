using NewsReport.Models;
using Microsoft.EntityFrameworkCore;

namespace NewsReport.Data
{
    public class NewsReportContext : DbContext
    {
        public DbSet<News> newsList{get; set;}
        public DbSet<NewsCategory> NewsCategory{get; set;}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=NewsReport.db");
        }
    }
}