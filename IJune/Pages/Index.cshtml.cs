﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using IJune.Models;
using IJune.Data;

namespace IJune.Pages

{
    public class IndexModel : PageModel
    {
        private readonly IJuneContext db;
        public IndexModel(IJuneContext db) => this.db = db;
        public List<Album> albums{get; set;} = new List<Album>();
        public Album featureSong;

        public string username {get; set;}
        public void OnGet()
        {
            username = "ชินพงศ์ อังสุโชติเมธี ณ อ.24";
            albums = db.Albums.ToList();
            featureSong = albums[new Random().Next(albums.Count)];
        }
    }
}
