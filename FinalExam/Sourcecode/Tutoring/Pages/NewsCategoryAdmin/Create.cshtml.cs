using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Tutoring.Data;
using Tutoring.Models;

namespace Tutoring.Pages.NewsCategoryAdmin
{
    public class CreateModel : PageModel
    {
        private readonly Tutoring.Data.TutoringContext _context;

        public CreateModel(Tutoring.Data.TutoringContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Tutoringbook Tutoringbook { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.newList.Add(Tutoringbook);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}