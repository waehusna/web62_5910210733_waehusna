using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using NewsReport.Data;
using NewsReport.Models;

namespace NewsReport.Pages.NewsCategoryAdmin
{
    public class CreateModel : PageModel
    {
        private readonly NewsReport.Data.NewsReportContext _context;

        public CreateModel(NewsReport.Data.NewsReportContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["NewsCategoryID"] = new SelectList(_context.NewsCategory, "NewsCategoryID", "ShortName");
            return Page();
        }

        [BindProperty]
        public News News { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.newsList.Add(News);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}