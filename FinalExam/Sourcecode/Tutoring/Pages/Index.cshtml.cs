﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore;
using Tutoring.Data;
using Tutoring.Models;

namespace Tutoring.Pages
{
    public class IndexModel : PageModel
    {
        private readonly Tutoring.Data.TutoringContext _context;

        public IndexModel(Tutoring.Data.TutoringContext context)
        {
            _context = context;
        }

        public IList<Tutoringbook> Tutoringbook { get; set;}

        public async Task OnGetAsync()
        {
            Tutoringbook = await _context.newList.ToListAsync();
                
        }
    }
    }