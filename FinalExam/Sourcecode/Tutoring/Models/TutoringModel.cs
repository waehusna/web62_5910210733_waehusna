using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Tutoring.Models
{
    public class Tutoringbook {
        public int TutoringbookID { get; set;}
        public string Subject { get; set;}
        [DataType(DataType.Date)]
        public string TutoringbookDate { get; set;}
        public string Location { get; set;}
        public float Number_hour { get; set;}
        public int price { get; set;}

        public string NewsUserId {get; set;}
        public NewsUser postUser {get; set;}
    }

    public class NewsUser : IdentityUser{
        public string FirstName { get; set;}
        public string LastName { get; set;}

        
 }

}