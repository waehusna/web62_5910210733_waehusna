using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Tutoring.Data;
using Tutoring.Models;

namespace Tutoring.Pages.NewsCategoryAdmin
{
    public class EditModel : PageModel
    {
        private readonly Tutoring.Data.TutoringContext _context;

        public EditModel(Tutoring.Data.TutoringContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tutoringbook Tutoringbook { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tutoringbook = await _context.newList.FirstOrDefaultAsync(m => m.TutoringbookID == id);

            if (Tutoringbook == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Tutoringbook).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TutoringbookExists(Tutoringbook.TutoringbookID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool TutoringbookExists(int id)
        {
            return _context.newList.Any(e => e.TutoringbookID == id);
        }
    }
}
