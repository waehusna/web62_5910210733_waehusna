using System;
using System.ComponentModel.DataAnnotations;

namespace IJune.Models
{
    public class Album
    {
        public int AlbumID {get; set;}
        public string AlbumName{get; set;}
        public string ArtistName{get; set;}

        [DataType(DataType.Date)]
        public DateTime ReleaseDate {get; set;}
        public Double Price {get; set;}

        public string AlbumImage {get; set;}
        public string SampleAudio {get; set;}

        public int stock {get; set;}

    }
}