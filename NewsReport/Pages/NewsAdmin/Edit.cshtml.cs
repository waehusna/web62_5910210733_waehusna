using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NewsReport.Data;
using NewsReport.Models;

namespace NewsReport.Pages.NewsAdmin
{
    public class EditModel : PageModel
    {
        private readonly NewsReport.Data.NewsReportContext _context;

        public EditModel(NewsReport.Data.NewsReportContext context)
        {
            _context = context;
        }

        [BindProperty]
        public News News { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            News = await _context.newsList
                .Include(n => n.NewsCat).FirstOrDefaultAsync(m => m.NewsID == id);

            if (News == null)
            {
                return NotFound();
            }
           ViewData["NewscategoryID"] = new SelectList(_context.NewsCategory, "NewscategoryID", "ShortName");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(News).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NewsExists(News.NewsID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool NewsExists(int id)
        {
            return _context.newsList.Any(e => e.NewsID == id);
        }
    }
}
