using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Tutoring.Data;
using Tutoring.Models;

namespace Tutoring.Pages.NewsCategoryAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly Tutoring.Data.TutoringContext _context;

        public DetailsModel(Tutoring.Data.TutoringContext context)
        {
            _context = context;
        }

        public Tutoringbook Tutoringbook { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tutoringbook = await _context.newList.FirstOrDefaultAsync(m => m.TutoringbookID == id);

            if (Tutoringbook == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
