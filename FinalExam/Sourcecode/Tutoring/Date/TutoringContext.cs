using Tutoring.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Tutoring.Data
{
    public class TutoringContext : IdentityDbContext<NewsUser>
    {
        public DbSet<Tutoringbook> newList { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Tutoring.db");
        }
    }
}